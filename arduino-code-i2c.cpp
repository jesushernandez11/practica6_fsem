#include <Wire.h>

#define VAREF 2.7273
#define I2C_SLAVE_ADDR 0X0A
#define BOARD_LED 13

float temperature = 0;

void i2c_received_handler(int count);
void i2c_request_handler(int count);
float read_temp(void);
float read_avg_temp(int count);

void setup(void){
  analogReference(EXTERNAL);


  Wire.begin(I2C_SLAVE_ADDR);
  Wire.onReceive(i2c_received_handler);
  Wire.onRequest(i2c_request_handler);

  Serial.begin(56600);
  pinMode(BOARD_LED, OUTPUT);
}

void i2c_request_handler(){
  Wire.write((byte*) &temperature, sizeof(float));
}

void i2c_received_handler(int count){
  char received = 0;
  while(Wire.available()){
    received = (char)Wire.read();
    digitalWrite(BOARD_LED,received? HIGH: LOW);
    Serial.println(received);
  }
}
float readd_temp(void){

  int vplus = analogRead(0);

  int vminus = analogRead(1);

  int vdiff = vplus - vminus;



  float temp = vdiff*VAREF/10.24f;
  return temp;
}

void loop(){
  temperature=read_temp();
  delay(100);
}