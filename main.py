import smbus2
import struct
import time 
import matplotlib.pyplot as plt
SLAVE_ADDR = 0x0A #I2C Address of Arduino 1

#Name of the file in wich the log is kept
LOG_FILE = './temp.log'

#Initialize the I2C bus
#RPI versopm 1 requires smbus.SMBus(0)
i2c = smbus2.SMBus(1)
temp_plot=[]
def readTemperature():
  try:
    msg = smbus2.i2c_msg.read(SLAVE_ADDR, 4)
    i2c.i2c_rdwr(msg)
    data = list(msg)
    ba = bytearray()
    for c in data:
      ba.append(int(c))
    temp = struct.unpack('<f', ba)
    print('Recieved temp:  {} = {}'.format(data, temp))
    print('Temperature: {} °C'.format(round(temp[0],1)))
    return temp
  except:
    return None

def plot_temp(temp):
  plt.plot(temp)
  plt.ylabel('Temperatures read')
  plt.ylim([10,50])
  plt.savefig('graph', format='png')
  plt.show()

def log_temp(temperature):
  try:
    with open(LOG_FILE, 'a') as fp:
      fp.write('{}\n'.format(round(temperature[0],1)))
  except:
    return
def read_temp_from_log():
  try:
    with open(LOG_FILE, 'r') as file:
      linea = file.readlines()
      for i in linea:
        cad=i.replace('\n', '')
        temp_plot.append(float(cad))
  except:
    return

def main():
  while True:
    try:
      cTemp = readTemperature()
      log_temp(cTemp)
      time.sleep(1)
    except KeyboardInterrupt:
      read_temp_from_log()
      plot_temp(temp_plot)
      return

if __name__ == '__main__':
  main()
