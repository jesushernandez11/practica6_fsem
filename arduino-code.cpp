#define VAREF 2.7273

float read_temp(void);
float read_avg_temp(int count);

void setup(void){

  analogReference(EXTERNAL);


  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

float read_temp(void){

  int vplus = analogRead(0);

  int vminus = analogRead(1);

  int vdiff = vplus - vminus;



  float temp = vdiff*VAREF/10.24f;
  return temp;
}

float read_avg_temp(int count){
  float avgtemp=0;
  for(int i=0;i<count;++i){
    avgtemp+=read_temp();
  }
  return avgtemp/count;
}

void loop(void){
  float temp = read_avg_temp(5);
  Serial.print((int)temp);
  Serial.println((int)(10*temp)%10);
  digitalWrite(13,HIGH);
  delay(250);
  digitalWrite(13,LOW);
  delay(250);
}